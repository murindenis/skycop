<?php

namespace App\Repository;

use App\Document\FlightData;
use Doctrine\ODM\MongoDB\DocumentRepository;

class FlightDataRepository extends DocumentRepository
{
    public function getCount() {
        return $this->createQueryBuilder()
            //->field('schedDepApt')->equals('USD')
            ->count()
            ->getQuery()
            ->execute();
    }

    public function getAirportFlightStatus() {
        //'LAX', 'LHR', 'GVAHH', 'US', 'USD', 'USSD', 'GDDD', 'QWERTY', 'YUIOD'

        $periodStart = new \DateTime('2012-01-01'); //$origDate
        $periodEnd   = new \DateTime('2018-12-25'); //$origDateUtc

        // $opAirlineCode аэропорт Х
        $airport     = 'LAX';

        // flightNumber 1-отменен/2-отложен/3-забронирован
        $status = 1;
        // diversionStatus причина QWERTY-отмены/YUIOD-отложенности
        $reason      = 'QWERTY';

        return $this->createQueryBuilder()
        //return $this->createQueryBuilder()
            ->field('origDate')->lte($periodEnd)
            ->field('origDateUtc')->gte($periodStart)
            ->field('opAirlineCode')->equals($airport)
            ->field('diversionStatus')->equals($reason)
            ->field('flightNumber')->equals(2)
            //->count()
            ->getQuery()
            ->execute();
    }

    public function getAggregate() {
        //'LAX', 'LHR', 'GVAHH', 'US', 'USD', 'USSD', 'GDDD', 'QWERTY', 'YUIOD'

        $periodStart = new \DateTime('2012-01-01'); //$origDate
        $periodEnd   = new \DateTime('2013-12-25'); //$origDateUtc

        // $opAirlineCode аэропорт Х
        $airport     = 'LAX';

        // flightNumber 1-отменен/2-отложен/3-забронирован
        $status = 1;
        // diversionStatus причина QWERTY-отмены/YUIOD-отложенности
        $reason      = 'QWERTY';

        //$dm = $this->createAggregationBuilder();

        /*return $dm->project()
                      ->includeFields(['origDate', 'origDateUtc', 'opAirlineCode', 'diversionStatus', 'flightNumber', 'diversionStatus', 'depGate', 'opFlightNumber'])
                    ->match()
                      ->field('origDate')->lte($periodEnd)
                      ->field('origDateUtc')->gte($periodStart)
                      ->field('opAirlineCode')->equals($airport)
                      ->field('flightNumber')->equals(2)
                    ->group()
                      ->field('id')
                      ->expression($dm->expr()
                          ->field('depGate')
                          ->expression('$depGate')
                      )
                      ->field('count')
                      ->sum('$opFlightNumber')
                    ->execute();*/

        $builder = $this->createAggregationBuilder();

        return $builder
            ->project()
                ->includeFields(['origDate', 'origDateUtc', 'opAirlineCode', 'diversionStatus', 'flightNumber', 'diversionStatus', 'depGate', 'opFlightNumber'])
            ->match()
                ->field('origDate')->lte($periodEnd)
                ->field('origDateUtc')->gte($periodStart)
                ->field('opAirlineCode')->equals($airport)
                ->field('flightNumber')->equals(2)
            ->facet()
                ->field('groupedByItemCount')
            ->pipeline(
                $this->createAggregationBuilder(FlightData::class)->group()
                    ->field('id')
                    ->expression($builder->expr()
                        ->field('depGate')
                        ->expression('$depGate')
                    )
                    ->sum(1)
            )
            ->execute()
            /*->field('groupedByYear')
            ->pipeline(
                $this->createAggregationBuilder(FlightData::class)->group()
                    ->field('id')
                    ->year('purchaseDate')
                    ->field('lowestValue')
                    ->min('$value')
                    ->field('highestValue')
                    ->max('$value')
                    ->field('totalValue')
                    ->sum('$value')
                    ->field('averageValue')
                    ->avg('$value')
            )*/
            ;
    }
}
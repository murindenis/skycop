<?php

namespace App\Controller;

use App\Document\FlightData;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class FlightDataController extends Controller
{
    /**
     * @Route("/create", name="create")
     */
    public function create()
    {
        //$dm = $this->get('doctrine_mongodb')->getManager();
        //dump(get_class($dm));die;

        return new JsonResponse(array('Status' => 'OK'));
    }

    /**
     * @Route("/show", name="show")
     */
    public function show()
    {
        $dm = $this->get('doctrine_mongodb');
        // Общее кол-со записей
        //$data = $dm->getRepository(FlightData::class)->getCount();

        // Сколько отмененных/отложенных/забронированных ресов по аэропорту X
        $data = $dm->getRepository(FlightData::class)->getAggregate();
        /*foreach ($data as $d) {
            dump($d);
        } die;*/

        dump($data); die; //dump($data);die;
        return new JsonResponse($data);
    }
}
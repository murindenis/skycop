<?php

namespace App\Service;

use App\Document\FlightData;
use Doctrine\ODM\MongoDB\DocumentManager;

class InsertDataService
{
    private $dm;

    public function __construct(DocumentManager $dm)
    {
        $this->dm = $dm;
    }

    public function run()
    {
        $dataString = ['LAX', 'LHR', 'GVAHH', 'US', 'USD', 'USSD', 'GDDD', 'QWERTY', 'YUIOD'];
        $dataDate   = [
            new \DateTime('2018-07-10 22:45'),
            new \DateTime('2017-07-11 09:15'),
            new \DateTime('2016-07-10 13:20'),
            new \DateTime('2015-07-10 13:20'),
            new \DateTime('2014-07-10 13:20'),
            new \DateTime('2013-07-10 13:20'),
            new \DateTime('2012-07-10 13:20'),
        ];

        for ($i = 1; $i < 25000000; $i++) {
            $data = new FlightData();
            $data
                ->setFightId(rand(1, 999))
                ->setAirlineCode($dataString[array_rand($dataString)])
                ->setFlightNumber(rand(1, 999))
                ->setSchedDepApt($dataString[array_rand($dataString)])
                ->setSchedArrApt($dataDate[array_rand($dataDate)])
                ->setSchedDepUtc($dataDate[array_rand($dataDate)])
                ->setSchedArrUtc($dataDate[array_rand($dataDate)])
                ->setSchedDepLocal($dataDate[array_rand($dataDate)])
                ->setSchedArrLocal($dataDate[array_rand($dataDate)])
                ->setDiversionStatus($dataString[array_rand($dataString)])
                ->setRecovFlightId(rand(1, 999))
                ->setOpAirlineCode($dataString[array_rand($dataString)])
                ->setOpFlightNumber(rand(1, 999))
                ->setAltDepApt($dataString[array_rand($dataString)])
                ->setAltArrApt($dataString[array_rand($dataString)])
                ->setDepTerminal($dataString[array_rand($dataString)])
                ->setDepGate($dataString[array_rand($dataString)])
                ->setArrTerminal($dataString[array_rand($dataString)])
                ->setArrGate($dataString[array_rand($dataString)])
                ->setBaggage($dataString[array_rand($dataString)])
                ->setServiceType($dataString[array_rand($dataString)])
                ->setAircraftType($dataString[array_rand($dataString)])
                ->setUnscheduled($dataString[array_rand($dataString)])
                ->setDepCountryCode($dataString[array_rand($dataString)])
                ->setArrCountryCode($dataString[array_rand($dataString)])
                ->setTailNumber(rand(1, 999))
                ->setRegAirlineCode($dataString[array_rand($dataString)])
                ->setRegFlightNumber(rand(1, 999))
                ->setNumLegs(rand(1, 999))
                ->setSeqNumber(rand(1, 999))
                ->setOrigDate($dataDate[array_rand($dataDate)])
                ->setOrigDateUtc($dataDate[array_rand($dataDate)])
                ->setPrevFltAlCode($dataString[array_rand($dataString)])
                ->setPrevFltNumber(rand(1, 999))
                ->setPrevFltSchedArrLocal($dataDate[array_rand($dataDate)])
                ->setPrevFltSchedArrUtc($dataDate[array_rand($dataDate)])
                ->setOutGateLocal($dataDate[array_rand($dataDate)])
                ->setOutGateUtc($dataDate[array_rand($dataDate)])
                ->setOutGateAccuracy($dataString[array_rand($dataString)])
                ->setOutGateSource($dataString[array_rand($dataString)])
                ->setInAirLocal($dataDate[array_rand($dataDate)])
                ->setInAirUtc($dataDate[array_rand($dataDate)])
                ->setInAirAccuracy($dataString[array_rand($dataString)])
                ->setInAirSource($dataString[array_rand($dataString)])
                ->setLandedLocal($dataString[array_rand($dataString)])
                ->setLandedUtc($dataString[array_rand($dataString)])
                ->setLandedAccuracy($dataString[array_rand($dataString)])
                ->setLandedSource($dataString[array_rand($dataString)])
                ->setInGateLocal($dataDate[array_rand($dataDate)])
                ->setInGateUtc($dataDate[array_rand($dataDate)])
                ->setInGateAccuracy($dataString[array_rand($dataString)])
                ->setInGateSource($dataString[array_rand($dataString)])
                ->setSchedDepAptNS($dataString[array_rand($dataString)])
                ->setSchedArrAptNS($dataString[array_rand($dataString)])
                ->setAltDepAptNS($dataString[array_rand($dataString)])
                ->setAltArrAptNS($dataString[array_rand($dataString)])
                ->setRevSchedDepLocal($dataString[array_rand($dataString)])
                ->setRevSchedDepUtc($dataString[array_rand($dataString)])
                ->setRevSchedArrLocal($dataString[array_rand($dataString)])
                ->setRevSchedArrUtc($dataString[array_rand($dataString)])
                ->setSuffix($dataString[array_rand($dataString)])
                ->setAircraftOwner($dataString[array_rand($dataString)])
                ->setIsGA($dataString[array_rand($dataString)])
                ->setDiversionType($dataString[array_rand($dataString)])
                ->setSimpleMaxAlt($dataString[array_rand($dataString)])
                ->setSimpleAvgAlt($dataString[array_rand($dataString)])
                ->setSimpleMaxSpeed(rand(1, 999))
                ->setSimpleAvgSpeed(rand(1, 999))
                ->setDistanceGC($dataString[array_rand($dataString)])
                ->setTimeAloft($dataString[array_rand($dataString)])
                ->setCancelled($dataString[array_rand($dataString)])
                ->setActTaxiTimeDep($dataString[array_rand($dataString)])
                ->setActTaxiTimeArr($dataString[array_rand($dataString)])
                ->setMinLateDeparted($dataString[array_rand($dataString)])
                ->setMinLateArrived($dataString[array_rand($dataString)]);

            //$dm = $this->get('doctrine_mongodb')->getManager();

            $this->dm->persist($data);
            if (($i % 10000) == 0) {
                $this->dm->flush();
                $this->dm->clear();
            }
            echo "$i\n";
        }
        //$this->dm->flush();
    }

}
<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(repositoryClass="App\Repository\FlightDataRepository")
 */
class FlightData
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\Field(type="int")
     * @MongoDB\Index(order="asc")
     */
    protected $fightId;

    /**
     * @MongoDB\Field(type="string")
     * @MongoDB\Index(order="asc")
     */
    protected $airlineCode;

    /**
     * @MongoDB\Field(type="int")
     * @MongoDB\Index(order="asc")
     */
    protected $flightNumber;

    /**
     * @MongoDB\Field(type="string")
     * @MongoDB\Index(order="asc")
     */
    protected $schedDepApt;

    /**
     * @MongoDB\Field(type="date")
     * @MongoDB\Index(order="asc")
     */
    protected $schedArrApt;

    /**
     * @MongoDB\Field(type="date")
     * @MongoDB\Index(order="asc")
     */
    protected $schedDepUtc;

    /**
     * @MongoDB\Field(type="date")
     * @MongoDB\Index(order="asc")
     */
    protected $schedArrUtc;

    /**
     * @MongoDB\Field(type="date")
     * @MongoDB\Index(order="asc")
     */
    protected $schedDepLocal;

    /**
     * @MongoDB\Field(type="date")
     * @MongoDB\Index(order="asc")
     */
    protected $schedArrLocal;

    /**
     * @MongoDB\Field(type="string")
     * @MongoDB\Index(order="asc")
     */
    protected $diversionStatus;

    /**
     * @MongoDB\Field(type="int")
     * @MongoDB\Index(order="asc")
     */
    protected $recovFlightId;

    /**
     * @MongoDB\Field(type="string")
     * @MongoDB\Index(order="asc")
     */
    protected $opAirlineCode;

    /**
     * @MongoDB\Field(type="int")
     * @MongoDB\Index(order="asc")
     */
    protected $opFlightNumber;

    /**
     * @MongoDB\Field(type="string")
     * @MongoDB\Index(order="asc")
     */
    protected $altDepApt;

    /**
     * @MongoDB\Field(type="string")
     * @MongoDB\Index(order="asc")
     */
    protected $altArrApt;

    /**
     * @MongoDB\Field(type="string")
     * @MongoDB\Index(order="asc")
     */
    protected $depTerminal;

    /**
     * @MongoDB\Field(type="string")
     * @MongoDB\Index(order="asc")
     */
    protected $depGate;

    /**
     * @MongoDB\Field(type="string")
     * @MongoDB\Index(order="asc")
     */
    protected $arrTerminal;

    /**
     * @MongoDB\Field(type="string")
     * @MongoDB\Index(order="asc")
     */
    protected $arrGate;

    /**
     * @MongoDB\Field(type="string")
     * @MongoDB\Index(order="asc")
     */
    protected $baggage;

    /**
     * @MongoDB\Field(type="string")
     * @MongoDB\Index(order="asc")
     */
    protected $serviceType;

    /**
     * @MongoDB\Field(type="string")
     * @MongoDB\Index(order="asc")
     */
    protected $aircraftType;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $unscheduled;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $depCountryCode;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $arrCountryCode;

    /**
     * @MongoDB\Field(type="int")
     */
    protected $tailNumber;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $regAirlineCode;

    /**
     * @MongoDB\Field(type="int")
     */
    protected $regFlightNumber;

    /**
     * @MongoDB\Field(type="int")
     */
    protected $numLegs;

    /**
     * @MongoDB\Field(type="int")
     */
    protected $seqNumber;

    /**
     * @MongoDB\Field(type="date")
     * @MongoDB\Index(order="asc")
     */
    protected $origDate;

    /**
     * @MongoDB\Field(type="date")
     * @MongoDB\Index(order="asc")
     */
    protected $origDateUtc;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $prevFltAlCode;

    /**
     * @MongoDB\Field(type="int")
     */
    protected $prevFltNumber;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $prevFltSchedArrLocal;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $prevFltSchedArrUtc;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $outGateLocal;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $outGateUtc;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $outGateAccuracy;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $outGateSource;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $inAirLocal;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $inAirUtc;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $inAirAccuracy;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $inAirSource;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $landedLocal;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $landedUtc;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $landedAccuracy;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $landedSource;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $inGateLocal;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $inGateUtc;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $inGateAccuracy;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $inGateSource;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $schedDepAptNS;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $schedArrAptNS;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $altDepAptNS;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $altArrAptNS;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $revSchedDepLocal;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $revSchedDepUtc;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $revSchedArrLocal;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $revSchedArrUtc;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $suffix;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $aircraftOwner;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $isGA;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $diversionType;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $simpleMaxAlt;

    /**
     * @MongoDB\Field(type="float")
     */
    protected $simpleAvgAlt;

    /**
     * @MongoDB\Field(type="int")
     */
    protected $simpleMaxSpeed;

    /**
     * @MongoDB\Field(type="int")
     */
    protected $simpleAvgSpeed;

    /**
     * @MongoDB\Field(type="float")
     */
    protected $distanceGC;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $timeAloft;

    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $cancelled;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $actTaxiTimeDep;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $actTaxiTimeArr;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $minLateDeparted;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $minLateArrived;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return FlightData
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFightId()
    {
        return $this->fightId;
    }

    /**
     * @param mixed $fightId
     * @return FlightData
     */
    public function setFightId($fightId)
    {
        $this->fightId = $fightId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAirlineCode()
    {
        return $this->airlineCode;
    }

    /**
     * @param mixed $airlineCode
     * @return FlightData
     */
    public function setAirlineCode($airlineCode)
    {
        $this->airlineCode = $airlineCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFlightNumber()
    {
        return $this->flightNumber;
    }

    /**
     * @param mixed $flightNumber
     * @return FlightData
     */
    public function setFlightNumber($flightNumber)
    {
        $this->flightNumber = $flightNumber;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSchedDepApt()
    {
        return $this->schedDepApt;
    }

    /**
     * @param mixed $schedDepApt
     * @return FlightData
     */
    public function setSchedDepApt($schedDepApt)
    {
        $this->schedDepApt = $schedDepApt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSchedArrApt()
    {
        return $this->schedArrApt;
    }

    /**
     * @param mixed $schedArrApt
     * @return FlightData
     */
    public function setSchedArrApt($schedArrApt)
    {
        $this->schedArrApt = $schedArrApt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSchedDepUtc()
    {
        return $this->schedDepUtc;
    }

    /**
     * @param mixed $schedDepUtc
     * @return FlightData
     */
    public function setSchedDepUtc($schedDepUtc)
    {
        $this->schedDepUtc = $schedDepUtc;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSchedArrUtc()
    {
        return $this->schedArrUtc;
    }

    /**
     * @param mixed $schedArrUtc
     * @return FlightData
     */
    public function setSchedArrUtc($schedArrUtc)
    {
        $this->schedArrUtc = $schedArrUtc;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSchedDepLocal()
    {
        return $this->schedDepLocal;
    }

    /**
     * @param mixed $schedDepLocal
     * @return FlightData
     */
    public function setSchedDepLocal($schedDepLocal)
    {
        $this->schedDepLocal = $schedDepLocal;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSchedArrLocal()
    {
        return $this->schedArrLocal;
    }

    /**
     * @param mixed $schedArrLocal
     * @return FlightData
     */
    public function setSchedArrLocal($schedArrLocal)
    {
        $this->schedArrLocal = $schedArrLocal;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDiversionStatus()
    {
        return $this->diversionStatus;
    }

    /**
     * @param mixed $diversionStatus
     * @return FlightData
     */
    public function setDiversionStatus($diversionStatus)
    {
        $this->diversionStatus = $diversionStatus;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRecovFlightId()
    {
        return $this->recovFlightId;
    }

    /**
     * @param mixed $recovFlightId
     * @return FlightData
     */
    public function setRecovFlightId($recovFlightId)
    {
        $this->recovFlightId = $recovFlightId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOpAirlineCode()
    {
        return $this->opAirlineCode;
    }

    /**
     * @param mixed $opAirlineCode
     * @return FlightData
     */
    public function setOpAirlineCode($opAirlineCode)
    {
        $this->opAirlineCode = $opAirlineCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOpFlightNumber()
    {
        return $this->opFlightNumber;
    }

    /**
     * @param mixed $opFlightNumber
     * @return FlightData
     */
    public function setOpFlightNumber($opFlightNumber)
    {
        $this->opFlightNumber = $opFlightNumber;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAltDepApt()
    {
        return $this->altDepApt;
    }

    /**
     * @param mixed $altDepApt
     * @return FlightData
     */
    public function setAltDepApt($altDepApt)
    {
        $this->altDepApt = $altDepApt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAltArrApt()
    {
        return $this->altArrApt;
    }

    /**
     * @param mixed $altArrApt
     * @return FlightData
     */
    public function setAltArrApt($altArrApt)
    {
        $this->altArrApt = $altArrApt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDepTerminal()
    {
        return $this->depTerminal;
    }

    /**
     * @param mixed $depTerminal
     * @return FlightData
     */
    public function setDepTerminal($depTerminal)
    {
        $this->depTerminal = $depTerminal;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDepGate()
    {
        return $this->depGate;
    }

    /**
     * @param mixed $depGate
     * @return FlightData
     */
    public function setDepGate($depGate)
    {
        $this->depGate = $depGate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getArrTerminal()
    {
        return $this->arrTerminal;
    }

    /**
     * @param mixed $arrTerminal
     * @return FlightData
     */
    public function setArrTerminal($arrTerminal)
    {
        $this->arrTerminal = $arrTerminal;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getArrGate()
    {
        return $this->arrGate;
    }

    /**
     * @param mixed $arrGate
     * @return FlightData
     */
    public function setArrGate($arrGate)
    {
        $this->arrGate = $arrGate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBaggage()
    {
        return $this->baggage;
    }

    /**
     * @param mixed $baggage
     * @return FlightData
     */
    public function setBaggage($baggage)
    {
        $this->baggage = $baggage;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getServiceType()
    {
        return $this->serviceType;
    }

    /**
     * @param mixed $serviceType
     * @return FlightData
     */
    public function setServiceType($serviceType)
    {
        $this->serviceType = $serviceType;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAircraftType()
    {
        return $this->aircraftType;
    }

    /**
     * @param mixed $aircraftType
     * @return FlightData
     */
    public function setAircraftType($aircraftType)
    {
        $this->aircraftType = $aircraftType;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUnscheduled()
    {
        return $this->unscheduled;
    }

    /**
     * @param mixed $unscheduled
     * @return FlightData
     */
    public function setUnscheduled($unscheduled)
    {
        $this->unscheduled = $unscheduled;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDepCountryCode()
    {
        return $this->depCountryCode;
    }

    /**
     * @param mixed $depCountryCode
     * @return FlightData
     */
    public function setDepCountryCode($depCountryCode)
    {
        $this->depCountryCode = $depCountryCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getArrCountryCode()
    {
        return $this->arrCountryCode;
    }

    /**
     * @param mixed $arrCountryCode
     * @return FlightData
     */
    public function setArrCountryCode($arrCountryCode)
    {
        $this->arrCountryCode = $arrCountryCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTailNumber()
    {
        return $this->tailNumber;
    }

    /**
     * @param mixed $tailNumber
     * @return FlightData
     */
    public function setTailNumber($tailNumber)
    {
        $this->tailNumber = $tailNumber;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegAirlineCode()
    {
        return $this->regAirlineCode;
    }

    /**
     * @param mixed $regAirlineCode
     * @return FlightData
     */
    public function setRegAirlineCode($regAirlineCode)
    {
        $this->regAirlineCode = $regAirlineCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegFlightNumber()
    {
        return $this->regFlightNumber;
    }

    /**
     * @param mixed $regFlightNumber
     * @return FlightData
     */
    public function setRegFlightNumber($regFlightNumber)
    {
        $this->regFlightNumber = $regFlightNumber;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumLegs()
    {
        return $this->numLegs;
    }

    /**
     * @param mixed $numLegs
     * @return FlightData
     */
    public function setNumLegs($numLegs)
    {
        $this->numLegs = $numLegs;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSeqNumber()
    {
        return $this->seqNumber;
    }

    /**
     * @param mixed $seqNumber
     * @return FlightData
     */
    public function setSeqNumber($seqNumber)
    {
        $this->seqNumber = $seqNumber;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrigDate()
    {
        return $this->origDate;
    }

    /**
     * @param mixed $origDate
     * @return FlightData
     */
    public function setOrigDate($origDate)
    {
        $this->origDate = $origDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrigDateUtc()
    {
        return $this->origDateUtc;
    }

    /**
     * @param mixed $origDateUtc
     * @return FlightData
     */
    public function setOrigDateUtc($origDateUtc)
    {
        $this->origDateUtc = $origDateUtc;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrevFltAlCode()
    {
        return $this->prevFltAlCode;
    }

    /**
     * @param mixed $prevFltAlCode
     * @return FlightData
     */
    public function setPrevFltAlCode($prevFltAlCode)
    {
        $this->prevFltAlCode = $prevFltAlCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrevFltNumber()
    {
        return $this->prevFltNumber;
    }

    /**
     * @param mixed $prevFltNumber
     * @return FlightData
     */
    public function setPrevFltNumber($prevFltNumber)
    {
        $this->prevFltNumber = $prevFltNumber;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrevFltSchedArrLocal()
    {
        return $this->prevFltSchedArrLocal;
    }

    /**
     * @param mixed $prevFltSchedArrLocal
     * @return FlightData
     */
    public function setPrevFltSchedArrLocal($prevFltSchedArrLocal)
    {
        $this->prevFltSchedArrLocal = $prevFltSchedArrLocal;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrevFltSchedArrUtc()
    {
        return $this->prevFltSchedArrUtc;
    }

    /**
     * @param mixed $prevFltSchedArrUtc
     * @return FlightData
     */
    public function setPrevFltSchedArrUtc($prevFltSchedArrUtc)
    {
        $this->prevFltSchedArrUtc = $prevFltSchedArrUtc;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOutGateLocal()
    {
        return $this->outGateLocal;
    }

    /**
     * @param mixed $outGateLocal
     * @return FlightData
     */
    public function setOutGateLocal($outGateLocal)
    {
        $this->outGateLocal = $outGateLocal;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOutGateUtc()
    {
        return $this->outGateUtc;
    }

    /**
     * @param mixed $outGateUtc
     * @return FlightData
     */
    public function setOutGateUtc($outGateUtc)
    {
        $this->outGateUtc = $outGateUtc;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOutGateAccuracy()
    {
        return $this->outGateAccuracy;
    }

    /**
     * @param mixed $outGateAccuracy
     * @return FlightData
     */
    public function setOutGateAccuracy($outGateAccuracy)
    {
        $this->outGateAccuracy = $outGateAccuracy;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOutGateSource()
    {
        return $this->outGateSource;
    }

    /**
     * @param mixed $outGateSource
     * @return FlightData
     */
    public function setOutGateSource($outGateSource)
    {
        $this->outGateSource = $outGateSource;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInAirLocal()
    {
        return $this->inAirLocal;
    }

    /**
     * @param mixed $inAirLocal
     * @return FlightData
     */
    public function setInAirLocal($inAirLocal)
    {
        $this->inAirLocal = $inAirLocal;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInAirUtc()
    {
        return $this->inAirUtc;
    }

    /**
     * @param mixed $inAirUtc
     * @return FlightData
     */
    public function setInAirUtc($inAirUtc)
    {
        $this->inAirUtc = $inAirUtc;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInAirAccuracy()
    {
        return $this->inAirAccuracy;
    }

    /**
     * @param mixed $inAirAccuracy
     * @return FlightData
     */
    public function setInAirAccuracy($inAirAccuracy)
    {
        $this->inAirAccuracy = $inAirAccuracy;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInAirSource()
    {
        return $this->inAirSource;
    }

    /**
     * @param mixed $inAirSource
     * @return FlightData
     */
    public function setInAirSource($inAirSource)
    {
        $this->inAirSource = $inAirSource;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLandedLocal()
    {
        return $this->landedLocal;
    }

    /**
     * @param mixed $landedLocal
     * @return FlightData
     */
    public function setLandedLocal($landedLocal)
    {
        $this->landedLocal = $landedLocal;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLandedUtc()
    {
        return $this->landedUtc;
    }

    /**
     * @param mixed $landedUtc
     * @return FlightData
     */
    public function setLandedUtc($landedUtc)
    {
        $this->landedUtc = $landedUtc;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLandedAccuracy()
    {
        return $this->landedAccuracy;
    }

    /**
     * @param mixed $landedAccuracy
     * @return FlightData
     */
    public function setLandedAccuracy($landedAccuracy)
    {
        $this->landedAccuracy = $landedAccuracy;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLandedSource()
    {
        return $this->landedSource;
    }

    /**
     * @param mixed $landedSource
     * @return FlightData
     */
    public function setLandedSource($landedSource)
    {
        $this->landedSource = $landedSource;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInGateLocal()
    {
        return $this->inGateLocal;
    }

    /**
     * @param mixed $inGateLocal
     * @return FlightData
     */
    public function setInGateLocal($inGateLocal)
    {
        $this->inGateLocal = $inGateLocal;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInGateUtc()
    {
        return $this->inGateUtc;
    }

    /**
     * @param mixed $inGateUtc
     * @return FlightData
     */
    public function setInGateUtc($inGateUtc)
    {
        $this->inGateUtc = $inGateUtc;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInGateAccuracy()
    {
        return $this->inGateAccuracy;
    }

    /**
     * @param mixed $inGateAccuracy
     * @return FlightData
     */
    public function setInGateAccuracy($inGateAccuracy)
    {
        $this->inGateAccuracy = $inGateAccuracy;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInGateSource()
    {
        return $this->inGateSource;
    }

    /**
     * @param mixed $inGateSource
     * @return FlightData
     */
    public function setInGateSource($inGateSource)
    {
        $this->inGateSource = $inGateSource;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSchedDepAptNS()
    {
        return $this->schedDepAptNS;
    }

    /**
     * @param mixed $schedDepAptNS
     * @return FlightData
     */
    public function setSchedDepAptNS($schedDepAptNS)
    {
        $this->schedDepAptNS = $schedDepAptNS;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSchedArrAptNS()
    {
        return $this->schedArrAptNS;
    }

    /**
     * @param mixed $schedArrAptNS
     * @return FlightData
     */
    public function setSchedArrAptNS($schedArrAptNS)
    {
        $this->schedArrAptNS = $schedArrAptNS;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAltDepAptNS()
    {
        return $this->altDepAptNS;
    }

    /**
     * @param mixed $altDepAptNS
     * @return FlightData
     */
    public function setAltDepAptNS($altDepAptNS)
    {
        $this->altDepAptNS = $altDepAptNS;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAltArrAptNS()
    {
        return $this->altArrAptNS;
    }

    /**
     * @param mixed $altArrAptNS
     * @return FlightData
     */
    public function setAltArrAptNS($altArrAptNS)
    {
        $this->altArrAptNS = $altArrAptNS;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRevSchedDepLocal()
    {
        return $this->revSchedDepLocal;
    }

    /**
     * @param mixed $revSchedDepLocal
     * @return FlightData
     */
    public function setRevSchedDepLocal($revSchedDepLocal)
    {
        $this->revSchedDepLocal = $revSchedDepLocal;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRevSchedDepUtc()
    {
        return $this->revSchedDepUtc;
    }

    /**
     * @param mixed $revSchedDepUtc
     * @return FlightData
     */
    public function setRevSchedDepUtc($revSchedDepUtc)
    {
        $this->revSchedDepUtc = $revSchedDepUtc;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRevSchedArrLocal()
    {
        return $this->revSchedArrLocal;
    }

    /**
     * @param mixed $revSchedArrLocal
     * @return FlightData
     */
    public function setRevSchedArrLocal($revSchedArrLocal)
    {
        $this->revSchedArrLocal = $revSchedArrLocal;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRevSchedArrUtc()
    {
        return $this->revSchedArrUtc;
    }

    /**
     * @param mixed $revSchedArrUtc
     * @return FlightData
     */
    public function setRevSchedArrUtc($revSchedArrUtc)
    {
        $this->revSchedArrUtc = $revSchedArrUtc;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSuffix()
    {
        return $this->suffix;
    }

    /**
     * @param mixed $suffix
     * @return FlightData
     */
    public function setSuffix($suffix)
    {
        $this->suffix = $suffix;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAircraftOwner()
    {
        return $this->aircraftOwner;
    }

    /**
     * @param mixed $aircraftOwner
     * @return FlightData
     */
    public function setAircraftOwner($aircraftOwner)
    {
        $this->aircraftOwner = $aircraftOwner;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getisGA()
    {
        return $this->isGA;
    }

    /**
     * @param mixed $isGA
     * @return FlightData
     */
    public function setIsGA($isGA)
    {
        $this->isGA = $isGA;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDiversionType()
    {
        return $this->diversionType;
    }

    /**
     * @param mixed $diversionType
     * @return FlightData
     */
    public function setDiversionType($diversionType)
    {
        $this->diversionType = $diversionType;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSimpleMaxAlt()
    {
        return $this->simpleMaxAlt;
    }

    /**
     * @param mixed $simpleMaxAlt
     * @return FlightData
     */
    public function setSimpleMaxAlt($simpleMaxAlt)
    {
        $this->simpleMaxAlt = $simpleMaxAlt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSimpleAvgAlt()
    {
        return $this->simpleAvgAlt;
    }

    /**
     * @param mixed $simpleAvgAlt
     * @return FlightData
     */
    public function setSimpleAvgAlt($simpleAvgAlt)
    {
        $this->simpleAvgAlt = $simpleAvgAlt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSimpleMaxSpeed()
    {
        return $this->simpleMaxSpeed;
    }

    /**
     * @param mixed $simpleMaxSpeed
     * @return FlightData
     */
    public function setSimpleMaxSpeed($simpleMaxSpeed)
    {
        $this->simpleMaxSpeed = $simpleMaxSpeed;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSimpleAvgSpeed()
    {
        return $this->simpleAvgSpeed;
    }

    /**
     * @param mixed $simpleAvgSpeed
     * @return FlightData
     */
    public function setSimpleAvgSpeed($simpleAvgSpeed)
    {
        $this->simpleAvgSpeed = $simpleAvgSpeed;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDistanceGC()
    {
        return $this->distanceGC;
    }

    /**
     * @param mixed $distanceGC
     * @return FlightData
     */
    public function setDistanceGC($distanceGC)
    {
        $this->distanceGC = $distanceGC;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTimeAloft()
    {
        return $this->timeAloft;
    }

    /**
     * @param mixed $timeAloft
     * @return FlightData
     */
    public function setTimeAloft($timeAloft)
    {
        $this->timeAloft = $timeAloft;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCancelled()
    {
        return $this->cancelled;
    }

    /**
     * @param mixed $cancelled
     * @return FlightData
     */
    public function setCancelled($cancelled)
    {
        $this->cancelled = $cancelled;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getActTaxiTimeDep()
    {
        return $this->actTaxiTimeDep;
    }

    /**
     * @param mixed $actTaxiTimeDep
     * @return FlightData
     */
    public function setActTaxiTimeDep($actTaxiTimeDep)
    {
        $this->actTaxiTimeDep = $actTaxiTimeDep;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getActTaxiTimeArr()
    {
        return $this->actTaxiTimeArr;
    }

    /**
     * @param mixed $actTaxiTimeArr
     * @return FlightData
     */
    public function setActTaxiTimeArr($actTaxiTimeArr)
    {
        $this->actTaxiTimeArr = $actTaxiTimeArr;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMinLateDeparted()
    {
        return $this->minLateDeparted;
    }

    /**
     * @param mixed $minLateDeparted
     * @return FlightData
     */
    public function setMinLateDeparted($minLateDeparted)
    {
        $this->minLateDeparted = $minLateDeparted;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMinLateArrived()
    {
        return $this->minLateArrived;
    }

    /**
     * @param mixed $minLateArrived
     * @return FlightData
     */
    public function setMinLateArrived($minLateArrived)
    {
        $this->minLateArrived = $minLateArrived;

        return $this;
    }
}
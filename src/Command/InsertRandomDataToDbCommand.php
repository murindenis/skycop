<?php

namespace App\Command;

use App\Service\InsertDataService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InsertRandomDataToDbCommand extends Command
{

    private $insertDataService;

    public function __construct(InsertDataService $insertDataService)
    {
        $this->insertDataService = $insertDataService;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('mongo:random')
            ->setDescription('Create random data');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Create random data',
            '==================',
        ]);

        $this->insertDataService->run();
    }
}